package hello.boardgames;

public class BoardGame {

    private long oidBoardGame;
    private String name;
    private String description;
    private String imageUrl;

    public long getOidBoardGame() {
        return oidBoardGame;
    }

    public void setOidBoardGame(long oidBoardGame) {
        this.oidBoardGame = oidBoardGame;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}