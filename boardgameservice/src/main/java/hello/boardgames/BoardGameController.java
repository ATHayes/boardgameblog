package hello.boardgames;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class BoardGameController {

  @RequestMapping("/boardgames")
  public List<BoardGame> greeting() {
    ArrayList<BoardGame> boardGames = new ArrayList<>();

    BoardGame boardGame1 = new BoardGame();
    boardGame1.setOidBoardGame(1);
    boardGame1.setName("Catan");
    boardGame1.setDescription(
            "Catan is a simple, approachable game, " +
            "which sees players compete for control of an unexplored island. " +
            "Violence is not an option here - players have to outsmart each other, by " +
            "settling beside the right resources, using event cards and making smart trades with other players. " +
            "Catan is an easy game for anyone to pick up and a fantastic introduction to modern board games.");
    boardGame1.setImageUrl("https://res.cloudinary.com/davbdgyux/image/upload/v1577337475/boardgameblog/catan_xhnwf2.jpg");

    BoardGame boardGame2 = new BoardGame();
    boardGame2.setOidBoardGame(1);
    boardGame2.setName("Castles of Burgundy");
    boardGame2.setDescription(
            "Castles of Burgundy is a little similar to Catan, " +
            "in that players compete economically to develop a settlement. " +
            "Players take turns in purchasing resources from a shared pool, " +
            "as they develop their own estates. Different resources work together in different ways to provide victory points. " +
            "Castles of Burgundy comes highly recommended for someone looking for something with a little more  " +
            "depth and freedom of options than Catan."
            );
    boardGame2.setImageUrl("https://res.cloudinary.com/davbdgyux/image/upload/v1577337427/boardgameblog/castles_of_burgundy_sgcufj.jpg");

    boardGames.add(boardGame1);
    boardGames.add(boardGame2);
    return boardGames;

  }
}