import React, {useEffect, useState} from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import axios from "axios";
import {MyFooter} from "../../shared-components/my-footer";
import {MyAppBar} from "../../shared-components/my-app-bar";
import Drawer from "@material-ui/core/Drawer";


const useStyles = makeStyles(theme => ({
    heroContent: {
        paddingTop:130,
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    },
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        height: "500px"
    },
    cardContent: {
        flexGrow: 1,
    }
}));

export default function BoardGameList() {
    const classes = useStyles();

    const [boardgames, setBoardgames] = useState([]);

    useEffect(() => {
        axios.get(`${process.env.REACT_APP_BACKEND_URL}/boardgames`)
            .then(response => {
                setBoardgames(response.data);
            })
            .catch(error => {

            });
    }, []);

    return (
        <React.Fragment>
            <CssBaseline />
            <MyAppBar/>
            <main className={"grid-main"}>
                <div className={classes.heroContent}>
                    <Container maxWidth="sm">
                        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                            Board Games
                        </Typography>
                        <Typography variant="h5" align="center" color="textSecondary" paragraph>
                            These are some great board games, in no particular order.
                        </Typography>

                    </Container>
                </div>
                <Container className={classes.cardGrid} maxWidth="md">
                    <Grid container spacing={4}>
                        {boardgames.map(boardgame => (
                            <Grid item key={boardgame} xs={12} sm={12} md={6}>
                                <Card className={classes.card}>
                                    <CardMedia
                                        className={classes.cardMedia}
                                        image={boardgame.imageUrl}
                                        title="Image title"
                                    />
                                    <CardContent className={classes.cardContent}>
                                        <Typography gutterBottom variant="h5" component="h2">
                                            {boardgame.name}
                                        </Typography>
                                        <Typography>
                                            {boardgame.description}
                                        </Typography>
                                    </CardContent>
                                    <CardActions>
                                        <Button size="small" color="primary">
                                            Edit
                                        </Button>
                                    </CardActions>
                                </Card>
                            </Grid>
                        ))}
                    </Grid>
                </Container>
            </main>

        </React.Fragment>
    );
}