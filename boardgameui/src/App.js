import React from 'react';
import BoardGameList from "./views/board-game-list/board-game-list"

function App() {
    return (
        <div className="App grid-container">
            <BoardGameList></BoardGameList>
        </div>
    );
}

export default App;
